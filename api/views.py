from django.shortcuts import render
from rest_framework import generics
from .models import MyModel
from .serializers import JsonDataSerializer
# Create your views here.

class Jsondatastorage(generics.ListCreateAPIView):
    queryset = MyModel.objects.all()
    serializer_class = JsonDataSerializer
